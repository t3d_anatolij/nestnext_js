import React from 'react';
import PropTypes from 'prop-types';

import { Button } from '../Button';

const SearchButton = ({ onClick, ...other }) => {
  return (
    <Button onClick={onClick} {...other}>
      <span className="icon-search icon-search-blue" />
    </Button>
  );
};

SearchButton.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default SearchButton;
