import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { keyCodes } from '../../constants';
import { DomUtils } from '../utils';
import * as _ from 'lodash';
import SearchButton from './SearchButton';
import SuggestionList from './SuggestionList';

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
      inputFocus: false,
      isAuth: props.isAuth,
      searchFocus: false,
      autoFocusSuggestionItem: false,
      loading: false,
      showSuggestionList: false,
      choiceMade: false,
    };

    this.anchorEl = React.createRef();
    this.inputRef = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    const regex = new RegExp(_.escapeRegExp(state.value), 'i');
    const { field, suggestions, variant, emptyMessageRender } = props;
    const { value, choiceMade } = state;
    let haveMatches;
    let newState = state;

    if (typeof field === 'string') {
      haveMatches = suggestions.some((item) => regex.exec(item[field]));
    }

    if (typeof field === 'object') {
      haveMatches = field.some((fieldItem) =>
        suggestions.some((item) => regex.exec(item[fieldItem])),
      );
    }

    if (variant !== 'search') {
      if (value && (suggestions.length || emptyMessageRender) && !choiceMade) {
        newState.loading = false;
        newState.showSuggestionList = true;
      }

      if (
        (!haveMatches && !emptyMessageRender && variant !== 'suggest') ||
        !value ||
        choiceMade
      ) {
        newState.showSuggestionList = false;
      }
    }

    return newState;
  }

  componentDidUpdate(prevProps) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      this.setState({ value });
    }
  }

  componentDidMount() {
    const { handleChangeValue, delay } = this.props;

    this.handleGetSuggestionDebounced = _.debounce((value) => {
      handleChangeValue(value);
      this.setState({ loading: false });
    }, delay);
  }

  componentWillUnmount() {
    this.handleGetSuggestionDebounced.cancel();
  }

  // handleInputFocus = () => {
  //   this.inputRef.current && this.inputRef.current.focus();
  // };

  handleChange = (e) => {
    const { value } = e.target;
    const { variant, onChange } = this.props;

    onChange(value);

    if (variant !== 'search') {
      this.setState({ value, choiceMade: false }, () => {
        DomUtils.setFocus(this.inputRef);
      });
    } else {
      this.setState({ value });
    }
  };

  handleFocus = () => {
    const { onInputFocus } = this.props;

    this.setState(
      { inputFocus: true, autoFocusSuggestionItem: false },
      onInputFocus,
    );
  };

  handleBlur = () => this.setState({ inputFocus: false });

  handleSearchFocus = () => this.setState({ searchFocus: true });

  handleSearchBlur = () => this.setState({ searchFocus: false });

  handleSearch = (e) => {
    e.preventDefault();
    e.stopPropagation();

    const {
      state: { value: rawValue },
      props: { value: query, onSearch, disabledSearch },
    } = this;

    const value = rawValue.trim();

    if (value !== query && !disabledSearch) {
      this.setState({
        showSuggestionList: false,
        choiceMade: true,
        autoFocusSuggestionItem: false,
      });
      onSearch(value);
    }
  };

  handleClean = () => {
    this.setState({
      value: '',
      showSuggestionList: false,
      choiceMade: false,
      autoFocusSuggestionItem: false,
    });
    this.props.onClean();
  };

  handleClose = () => {
    this.setState({
      showSuggestionList: false,
      choiceMade: true,
      autoFocusSuggestionItem: false,
    });
    this.props.onClose();
  };

  handleKeyUp = (e) => {
    e.preventDefault();
    let { value, choiceMade, showSuggestionList } = this.state;
    const { minLength, variant, disabledSearch } = this.props;
    const { TAB, ENTER_KEY: ENTER, ESC, DOWN, BACKSPACE, DELETE } = keyCodes;

    if (variant === 'search' || (disabledSearch && e.keyCode === ENTER)) {
      return;
    }

    switch (e.keyCode) {
      case ENTER:
      case TAB:
      case ESC:
        {
          this.handleClose();
        }
        break;
      case DOWN:
        this.setState({ autoFocusSuggestionItem: true });
        return;
      case BACKSPACE:
      case DELETE:
        if (
          value &&
          value.length <= minLength &&
          showSuggestionList &&
          !choiceMade
        ) {
          this.setState({
            choiceMade: false,
            showSuggestionList: false,
            autoFocusSuggestionItem: false,
          });
        }
        break;
      default:
        break;
    }
    value = value.trim();

    if (value && value.length > minLength) {
      this.setState({ loading: true });
      this.handleGetSuggestionDebounced(value);
    }
  };

  handleListClick = (e, itemValue, { item }) => {
    e.preventDefault();
    // eslint-disable-next-line react/prop-types
    const { field, onSelection } = this.props;
    const regex = new RegExp(_.escapeRegExp(itemValue), 'i');
    const suggestionField =
      typeof field === 'string'
        ? field
        : field.find((fieldItem) => regex.exec(item[fieldItem]));
    const value = item[suggestionField];

    DomUtils.setFocus(this.inputRef);

    this.setState({ value, choiceMade: true, autoFocusSuggestionItem: false });
    onSelection(item, suggestionField);
  };

  handleListKeyDown = (e) => {
    const { LEFT, RIGHT } = keyCodes;
    const charCode = DomUtils.getCharCode(e);

    if (charCode === LEFT || charCode === RIGHT) {
      DomUtils.setFocus(this.inputRef);
    }
  };

  render() {
    const {
      // emptyMessageRender,
      placeholder,
      // className,
      // variant,
      suggestions,
      field,
      autoCompleteVariant,
      itemRender,
      searchType,
      dataItemKey,
      isAuth,
      // itemsLoading,
    } = this.props;
    const {
      value,
      inputFocus,
      searchFocus,
      // loading,
      // List,
      // autoFocusSuggestionItem,
    } = this.state;

    // const isMobile = searchType === 'mobile';

    // const rootClasses = classNames('search-box');
    const inputClasses = classNames('search-box__body', {
      'search-box__body--active': inputFocus,
    });
    const submitButtonClasses = classNames('search-box__actions', {
      'search-box__actions--active': searchFocus,
    });

    return (
      <>
        <form
          className={
            !isAuth
              ? 'search-box marginSearch'
              : 'search-box marginSearchIsAuth'
          }
          onSubmit={this.handleSearch}
          ref={this.anchorEl}
        >
          <label className={inputClasses}>
            {searchType === 'standard' && (
              <div className={submitButtonClasses}>
                <SearchButton
                  onFocus={this.handleSearchFocus}
                  onBlur={this.handleSearchBlur}
                />
              </div>
            )}
            <div className="search-box__input-container">
              {/*{!value && <span className="search-box__placeholder">{placeholder}</span>}*/}
              <input
                aria-label={placeholder}
                className={classNames('search-box__input')}
                value={value}
                ref={this.inputRef}
                onChange={this.handleChange}
                // onFocus={this.handleFocus}
                // onBlur={this.handleBlur}
                onKeyUp={this.handleKeyUp}
              />
            </div>
            {value && (
              <SuggestionList
                search={value}
                anchorEl={this.anchorEl}
                suggestions={suggestions}
                field={field}
                variant={autoCompleteVariant}
                itemRender={itemRender}
                dataItemKey={dataItemKey}
                onClick={this.handleListClick}
                onClose={this.handleClose}
                onKeyDown={this.handleListKeyDown}
              />
            )}
          </label>
        </form>
      </>
    );
  }
}

SearchBox.propTypes = {
  autoComplete: PropTypes.bool,
  autoCompleteVariant: PropTypes.oneOf(['none', 'other']),
  className: PropTypes.string,
  dataItemKey: PropTypes.string,
  delay: PropTypes.number,
  disabledSearch: PropTypes.bool,
  emptyMessageRender: PropTypes.func,
  field: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  handleChangeValue: PropTypes.func,
  itemRender: PropTypes.func,
  itemsLoading: PropTypes.bool,
  minLength: PropTypes.number,
  onChange: PropTypes.func,
  onClean: PropTypes.func,
  onClose: PropTypes.func,
  onInputFocus: PropTypes.func,
  onSearch: function (props, propName) {
    if (
      props['disabledSearch'] === false &&
      (props[propName] === undefined || typeof props[propName] !== 'function')
    ) {
      return new Error(`Please provide a "${propName}" function!`);
    }
  },
  placeholder: PropTypes.node,
  searchType: PropTypes.oneOf(['standard', 'mobile']),
  suggestions: PropTypes.arrayOf(Object),
  value: PropTypes.string,
  variant: PropTypes.oneOf(['search', 'autocomplete', 'suggest']),
  isAuth: PropTypes.bool,
};

SearchBox.defaultProps = {
  autoComplete: false,
  delay: 200,
  variant: 'search',
  minLength: 2,
  autoCompleteVariant: 'other',
  suggestions: [],
  handleChangeValue: () => {},
  onInputFocus: () => {},
  onSelection: () => {},
  onChange: () => {},
  onClose: () => {},
  onClean: () => {},
  searchType: 'standard',
  value: '',
  disabledSearch: false,
};

SearchBox.displayName = 'SearchBox';

export default SearchBox;
