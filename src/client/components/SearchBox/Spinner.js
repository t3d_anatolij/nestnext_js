import React from 'react';

const Spinner = () => <div className="spinner-border text-light" />;

Spinner.displayName = 'Spinner';

export default Spinner;
