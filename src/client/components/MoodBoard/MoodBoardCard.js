import PropTypes from 'prop-types';
import React from 'react';
import { Thumbnail } from '../Thumbnail';

const MoodBoardCard = ({ title, desc, author, url, actions }) => {
  const componnentValue = 'test';
  return (
    <div className="moodBoardCard">
      <div className="moodBoardCard-wrapper">
        <div className="moodBoardCard-wrapper-header">
          <div className="moodBoardCard-wrapper-header-text">
            <h3>{title}</h3>
            <h4>{desc}</h4>
          </div>
          <div className="moodBoardCard-wrapper-header-author">
            <div className="moodBoardCard-wrapper-header-author-text">
              <h3>{author.name}</h3>
            </div>
            <Thumbnail className="thumbnail-moodBoard">
              <img src={author.img} alt="avatar" />
            </Thumbnail>
          </div>
        </div>
        <img className="moodBoardCard-wrapper-image" src={url} alt="img" />
        <div className="moodBoardCard-wrapper-footer">
          {actions(componnentValue)}
        </div>
      </div>
    </div>
  );
};

MoodBoardCard.propTypes = {
  onClick: PropTypes.func,
  image: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
  author: PropTypes.string,
  url: PropTypes.string,
};

export default MoodBoardCard;
