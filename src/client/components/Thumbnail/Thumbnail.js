import classNames from 'classnames';
import PropTypes from 'prop-types';

const Thumbnail = ({ type, disabled, onClick, className, children }) => {
  const handleClick = (e) => {
    if (disabled) {
      e.preventDefault();
      return;
    }

    if (onClick) {
      onClick(e);
    }
  };

  return (
    <button
      type={type}
      className={classNames('thumbnail', className)}
      disabled={disabled}
      onClick={handleClick}
    >
      {children}
    </button>
  );
};

Thumbnail.propTypes = {
  type: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.any,
};

export default Thumbnail;
