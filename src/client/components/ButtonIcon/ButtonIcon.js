import classNames from 'classnames';
import PropTypes from 'prop-types';

const ButtonIcon = ({
  type,
  icon,
  title,
  disabled,
  onClick,
  className,
  children,
  socialText,
}) => {
  const handleClick = (e) => {
    if (disabled) {
      e.preventDefault();
      return;
    }

    if (onClick) {
      onClick(e);
    }
  };

  return (
    <button
      type={type}
      className={classNames('wise-button wise-button-icon', className)}
      disabled={disabled}
      onClick={handleClick}
    >
      {icon}
      {title && <div className="wise-button-icon__text">{title}</div>}
      {socialText && (
        <div className="wise-button-icon__moodBoard">{socialText}</div>
      )}
    </button>
  );
};

ButtonIcon.propTypes = {
  type: PropTypes.string,
  icon: PropTypes.element,
  title: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.any,
};

export default ButtonIcon;
