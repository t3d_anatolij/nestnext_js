import classNames from 'classnames';
import PropTypes from 'prop-types';

const Button = ({ type, children, disabled, onClick, className}) => {
  const handleClick = (e) => {
    if (disabled) {
      e.preventDefault();
      return;
    }

    if (onClick) {
      onClick(e);
    }
  };

  return (
    <button
      type={type}
      className={classNames('wise-button', className)}
      disabled={disabled}
      onClick={handleClick}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  type: PropTypes.string,
  children: PropTypes.any,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  className: PropTypes.string,
};

export default Button;
