import { useState } from 'react';
import { TabContent, TabPane } from 'reactstrap';
import classNames from 'classnames';
import { Button } from '../Button';
import SigninForm from './SigninForm';
import SignupForm from './SignupForm';

const tabEnum = {
  logIn: 'log-in',
  signUp: 'sign-up',
};

const Popup = () => {
  const [activeTab, setActiveTab] = useState(tabEnum.logIn);

  return (
    <div className="wise-auth-popup">
      <div className="wise-auth-popup__tabs">
        <Button
          className={classNames({
            'wise-button-outline': activeTab === tabEnum.logIn,
          })}
          onClick={() => setActiveTab(tabEnum.logIn)}
        >
          Log in
        </Button>
        <Button
          className={classNames({
            'wise-button-outline': activeTab === tabEnum.signUp,
          })}
          onClick={() => setActiveTab(tabEnum.signUp)}
        >
          Sign up
        </Button>
      </div>
      <TabContent activeTab={activeTab}>
        <TabPane tabId={tabEnum.logIn}>
          <SigninForm />
        </TabPane>
        <TabPane tabId={tabEnum.signUp}>
          <SignupForm />
        </TabPane>
      </TabContent>
    </div>
  );
};

export default Popup;
