import { Input } from 'components/Input';
import { Button } from 'components/Button';

const SignupForm = () => {
  return (
    <form action="" className="wise-auth-popup__form">
      <Input className="mb-3" placeholder="email*" required />
      <Input className="mb-3" placeholder="password*" required />
      <Input placeholder="confirm password" required />
      <Button type="submit"> Next </Button>
    </form>
  );
};

export default SignupForm;
