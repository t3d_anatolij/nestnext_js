import { Input } from 'components/Input';
import { Button } from 'components/Button';

const SigninForm = () => {
  return (
    <form action="" className="wise-auth-popup__form">
      <Input className="mb-3" placeholder="email*" required />
      <Input placeholder="password*" required />
      <Button type="submit"> Log in </Button>
      <Button
        className="wise-button-link text-decoration-underline fs-6"
        onClick={() => {}}
      >
        I forgot my password
      </Button>
    </form>
  );
};

export default SigninForm;
