import PropTypes from 'prop-types';
import classNames from 'classnames';

const Card = ({ children, className }) => {
  return <div className={classNames('wise-card', className)}>{children}</div>;
};

const CardTitle = ({ className, children }) => {
  return (
    <div className={classNames('wise-card__title', className)}>{children}</div>
  );
};

const CardSubtitle = ({ className, children }) => {
  return (
    <div className={classNames('wise-card__subtitle', className)}>
      {children}
    </div>
  );
};

const props = {
  children: PropTypes.any,
  className: PropTypes.string,
};

Card.propTypes = {
  ...props,
};

CardTitle.propTypes = {
  ...props,
};

CardSubtitle.propTypes = {
  ...props,
};

export { CardTitle, CardSubtitle };

export default Card;
