import PropTypes from 'prop-types';
import classNames from 'classnames';

const Link = ({ to, children, className, onClick }) => {
  const handleClick = (e) => {
    e.preventDefault();
    onClick && onClick(e);
  };

  return (
    <a
      className={classNames('wise-link', className)}
      href={to}
      onClick={handleClick}
    >
      {children}
    </a>
  );
};

Link.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.node]).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Link;
