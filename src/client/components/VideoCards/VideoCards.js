import PropTypes from 'prop-types';

import React from 'react';
import { VideoCard } from './components/VideoCard';

const VideoCards = () => {
  return (
    <div className="videoCards">
      <VideoCard className="bg-main" />
      <VideoCard className="bg-main" />
      <VideoCard className="bg-main" />
      <VideoCard className="bg-main" />
    </div>
  );
};

VideoCards.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  bgColor: PropTypes.string,
};

export default VideoCards;
