import PropTypes from 'prop-types';
import classNames from 'classnames';

const Input = ({
  onChange,
  value,
  className,
  type,
  placeholder,
  required,
  error,
}) => {
  const handleChange = (e) => {
    const { value } = e.target;
    if (onChange) {
      onChange(value);
    }
  };

  return (
    <input
      placeholder={placeholder}
      className={classNames('wise-input', className, {
        required: required,
        error: error,
      })}
      onChange={handleChange}
      value={value}
      type={type}
    />
  );
};

Input.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  setValue: PropTypes.string.isRequired,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  error: PropTypes.bool,
};

Input.defaultProps = {
  type: 'text',
  placeholder: 'Enter text',
  required: false,
  error: false,
};

export default Input;
