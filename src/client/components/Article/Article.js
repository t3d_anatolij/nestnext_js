import PropTypes from 'prop-types';
import React from 'react';
import { Thumbnail } from '../Thumbnail';
import { Button } from '../Button';

const Article = ({ title, desc, author, url, actions }) => {
  const componnentValue = 'test';
  return (
    <div className="article">
      <div className="article-wrapper">
        <div className="article-wrapper-image">
          <img src="/images/article-img.png" alt="img" />
          <div className="article-wrapper-image-btn">
            <Button className="wise-button-standard">Read Article</Button>
          </div>
          <div className="article-wrapper-image-author">
            <Thumbnail className="thumbnail-article">
              <img src="/images/avatar1.png" alt="avatar" />
            </Thumbnail>
            <div className="article-wrapper-image-author-text">
              <h3>{author}</h3>
            </div>
          </div>
        </div>
        <div className="article-wrapper-content">
          <div className="article-wrapper-content-text">
            <h3>{title}</h3>
            <h4>{desc}</h4>
          </div>
          {actions(componnentValue)}
        </div>
      </div>
    </div>
  );
};

Article.propTypes = {
  onClick: PropTypes.func,
  image: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
  author: PropTypes.string,
  url: PropTypes.string,
};

export default Article;
