import PropTypes from 'prop-types';
import React from 'react';
import { Thumbnail } from '../Thumbnail';
import { Button } from '../Button';

const Podcast = ({ title, desc, guest, author, language, url, actions }) => {
  const componentValue = 'test';
  return (
    <div className="podcast">
      <div className="podcast-wrapper">
        <div className="podcast-wrapper-image">
          <img src="/images/podcast.png" alt="img" />
          <div className="podcast-wrapper-image-btn">
            <Button className="wise-button-standard">Listen to Podcast</Button>
          </div>
          <div className="podcast-wrapper-image-footer">
            <Thumbnail className="thumbnail-podcast">
              <img src="/images/language-en.svg" alt="en" />
            </Thumbnail>
            <div className="podcast-wrapper-image-footer-author">
              <div className="podcast-wrapper-image-footer-author-text">
                <h3>{author}</h3>
              </div>
              <Thumbnail className="thumbnail-podcast">
                <img src="/images/avatar1.png" alt="avatar" />
              </Thumbnail>
            </div>
          </div>
        </div>
        <div className="podcast-wrapper-content">
          <div className="podcast-wrapper-content-text">
            <h3>{title}</h3>
            <h4>{desc}</h4>
            <h3>Guest</h3>
            <h4>{guest}</h4>
            <h3>Language</h3>
            <h4>{language}</h4>
          </div>
          {actions(componentValue)}
        </div>
      </div>
    </div>
  );
};

Podcast.propTypes = {
  onClick: PropTypes.func,
  image: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
  author: PropTypes.string,
  guest: PropTypes.string,
  language: PropTypes.string,
  url: PropTypes.string,
};

export default Podcast;
