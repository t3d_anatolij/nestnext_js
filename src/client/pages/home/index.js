import {
  MoodBoardCard,
  Slides,
  Card,
  CardTitle,
  CardSubtitle,
  Article,
  Podcast,
} from 'components';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'components/Button';
import { ButtonIcon } from 'components/ButtonIcon';
import React from 'react';

import { useRouter } from 'next/router';

const moodBoards = [
  {
    title: 'Mood board',
    desc: 'Awesome desct',
    author: {
      name: 'Marta Stuart',
      img: '/images/avatar1.png',
    },
    url: '/images/moodBoard1.png',
    likes: '1.3k',
    views: '243k',
    id: 'test',
  },
  {
    title: 'Mood board2',
    desc: 'Awesome desct2',
    author: {
      name: 'Marta Stuart',
      img: '/images/avatar1.png',
    },
    url: '/images/moodBoard1.png',
    likes: '1.2k',
    views: '215k',
    id: 'test2',
  },
];

const introVideoCards = [
  {
    title: 'Dreamers',
    subtitle: 'Build the house of your dreams',
  },
  {
    title: 'Designers',
    subtitle: 'Create your best projects ever',
  },
  {
    title: 'Contractors',
    subtitle: 'Land the most awesome jobs',
  },
  {
    title: 'Companies/Universities',
    subtitle: 'Manage everything in your browser',
  },
];

const videoCards = [
  {
    title: 'Video Card',
    desc: 'Awesome desct',
    author: {
      name: 'Marta Stuart',
      img: '/images/avatar1.png',
    },
    url: '/images/videoCard.png',
    likes: '2k',
    views: '115k',
    id: 'test',
  },
  {
    title: 'Video Card',
    desc: 'Awesome desct',
    author: {
      name: 'Marta Stuart',
      img: '/images/avatar1.png',
    },
    url: '/images/videoCard.png',
    likes: '1.8k',
    views: '355k',
    id: 'test',
  },
];
const article = [
  {
    title: 'Highest building how does it done?',
    desc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit velit in ante feugiat, eu venenatis risus semper. Phasellus accumsan magna ac faucibus feugiat. Etiam id augue in tellus pulvinar malesuada vitae ac nisi. Nulla scelerisque nisl ut velit placerat, nec pellentesque \n
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit velit in ante feugiat, eu venenatis risus semper. Phasellus accumsan magna ac faucibus feugiat. Etiam id augue in tellus pulvinar malesuada vitae ac nisi.`,
    author: 'Marta Stuart',
    url: '',
    likes: '2k',
    views: '115k',
    id: 'test',
  },
  {
    title: 'Highest building how does it done?',
    desc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit velit in ante feugiat, eu venenatis risus semper. Phasellus accumsan magna ac faucibus feugiat. Etiam id augue in tellus pulvinar malesuada vitae ac nisi. Nulla scelerisque nisl ut velit placerat, nec pellentesque
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit velit in ante feugiat, eu venenatis risus semper. Phasellus accumsan magna ac faucibus feugiat. Etiam id augue in tellus pulvinar malesuada vitae ac nisi.`,
    author: 'Marta Stuart',
    url: '',
    likes: '2k',
    views: '115k',
    id: 'test',
  },
];
const podcast = [
  {
    title: 'Title',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit velit in ante feugiat, eu venenatis risus semper. Phasellus accumsan magna ac faucibus feugiat. Etiam id augue in tellus pulvinar malesuada vitae ac nisi.',
    guest: `John Williams`,
    author: 'Marta Stuart',
    language: 'English',
    url: '',
    likes: '2k',
    views: '115k',
    id: 'test',
  },
  {
    title: 'Title',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit velit in ante feugiat, eu venenatis risus semper. Phasellus accumsan magna ac faucibus feugiat. Etiam id augue in tellus pulvinar malesuada vitae ac nisi.',
    guest: `John Williams`,
    author: 'Marta Stuart',
    language: 'French',
    url: '',
    likes: '2k',
    views: '115k',
    id: 'test',
  },
];

const isAuth = true;

export default function Home() {
  const router = useRouter();
  const onWatchClick = (id) => {
    router.push(`/moodboard/${id}`);
  };
  const onVideoClick = (id) => {
    router.push(`/video/${id}`);
  };
  return (
    <Container style={{ padding: 0 }}>
      <Row>
        <Slides />
      </Row>
      <div className="marginBottom">
        <Row xs={2} md={2} lg={4}>
          {introVideoCards.map(({ title, subtitle }, index) => (
            <Col key={index}>
              <Card className="bg-main">
                <div>
                  <CardTitle>{title}</CardTitle>
                  <CardSubtitle>{subtitle}</CardSubtitle>
                </div>
                <div className="card-btn">
                  <Button className={'wise-button-standard'}>Watch</Button>
                </div>
              </Card>
            </Col>
          ))}
        </Row>
      </div>
      <div className="marginBottom">
        <Row xs={1} md={1} lg={2}>
          {moodBoards.map((board, index) => (
            <Col key={index}>
              <MoodBoardCard
                key={index}
                {...board}
                actions={() => {
                  return (
                    <>
                      <Button
                        className="wise-button-outline"
                        onClick={() => onWatchClick(board.id)}
                      >
                        Watch
                      </Button>
                      <div className="moodBoardCard-wrapper-footer-social">
                        <ButtonIcon
                          icon={
                            <span className="icon-share_empty color-main" />
                          }
                        />
                        {isAuth && (
                          <ButtonIcon
                            icon={
                              <span className="icon-watch_empty color-main" />
                            }
                            socialText={board.views}
                          />
                        )}
                        <ButtonIcon
                          className={!isAuth && 'moodBoardCard-isAuth'}
                          icon={<span className="icon-like_empty color-main" />}
                          socialText={isAuth && board.likes}
                        />
                      </div>
                    </>
                  );
                }}
              />
            </Col>
          ))}
        </Row>
        <div className="btn-more">
          <Button className="wise-button-more">More</Button>
        </div>
      </div>
      <div className="marginBottom">
        <Row xs={1} md={1} lg={2}>
          {videoCards.map((board, index) => (
            <Col key={index}>
              <MoodBoardCard
                key={index}
                {...board}
                actions={() => {
                  return (
                    <>
                      <Button
                        className="wise-button-outline"
                        onClick={() => onWatchClick(board.id)}
                      >
                        Watch
                      </Button>
                      <div className="moodBoardCard-wrapper-footer-social">
                        <ButtonIcon
                          icon={
                            <span className="icon-share_empty color-main" />
                          }
                        />
                        {isAuth && (
                          <ButtonIcon
                            icon={
                              <span className="icon-watch_empty color-main" />
                            }
                            socialText={board.views}
                          />
                        )}
                        <ButtonIcon
                          className={!isAuth && 'moodBoardCard-isAuth'}
                          icon={<span className="icon-like_empty color-main" />}
                          socialText={isAuth && board.likes}
                        />
                      </div>
                    </>
                  );
                }}
              />
            </Col>
          ))}
        </Row>
        <div className="btn-more">
          <Button className="wise-button-more">More</Button>
        </div>
      </div>
      <div className="marginBottom">
        <Row xs={1} md={1} lg={2}>
          {article.map((article, index) => (
            <Col key={index}>
              <Article
                key={index}
                {...article}
                actions={() => {
                  return (
                    <>
                      <div className="article-wrapper-content-footer">
                        <ButtonIcon
                          icon={
                            <span className="icon-share_empty color-main" />
                          }
                        />
                        <div
                          className={
                            isAuth
                              ? 'article-wrapper-content-footer-social'
                              : 'article-wrapper-content-footer-social article-isAuth'
                          }
                        >
                          {isAuth && (
                            <ButtonIcon
                              icon={
                                <span className="icon-watch_empty color-main" />
                              }
                              socialText={article.views}
                            />
                          )}
                          <ButtonIcon
                            icon={
                              <span className="icon-like_empty color-main" />
                            }
                            socialText={isAuth && article.likes}
                          />
                        </div>
                      </div>
                    </>
                  );
                }}
              />
            </Col>
          ))}
        </Row>
        <div className="btn-more">
          <Button className="wise-button-more">More</Button>
        </div>
      </div>
      <div className="marginBottom">
        <Row xs={1} md={1} lg={2}>
          {podcast.map((podcast, index) => (
            <Col key={index}>
              <Podcast
                key={index}
                {...podcast}
                actions={() => {
                  return (
                    <>
                      <div className="podcast-wrapper-content-footer">
                        <ButtonIcon
                          icon={
                            <span className="icon-share_empty color-main" />
                          }
                        />
                        <div
                          className={
                            isAuth
                              ? 'podcast-wrapper-content-footer-social'
                              : 'podcast-wrapper-content-footer-social podcast-isAuth'
                          }
                        >
                          {isAuth && (
                            <ButtonIcon
                              icon={
                                <span className="icon-watch_empty color-main" />
                              }
                              socialText={podcast.views}
                            />
                          )}
                          <ButtonIcon
                            icon={
                              <span className="icon-like_empty color-main" />
                            }
                            socialText={isAuth && podcast.likes}
                          />
                        </div>
                      </div>
                    </>
                  );
                }}
              />
            </Col>
          ))}
        </Row>
        <div className="btn-more">
          <Button className="wise-button-more">More</Button>
        </div>
      </div>
    </Container>
  );
}
