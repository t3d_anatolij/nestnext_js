import PropTypes from 'prop-types';
import { Header } from '../components';
import { Footer } from '../components';
import '../styles/main.scss';

export default function App({ Component, pageProps }) {
  return (
    <>
      <Header
        title="TeamWise"
        logo={
          <span className="icon-Logo">
            <span className="path1" />
            <span className="path2" />
            <span className="path3" />
          </span>
        }
      />
      <Component {...pageProps} />
      <Footer />
    </>
  );
}

App.propTypes = {
  Component: PropTypes.element,
  pageProps: PropTypes.object,
};
