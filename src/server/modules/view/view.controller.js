import { Controller, Get, Bind, Dependencies, Res, Req } from '@nestjs/common';

import { ViewService } from './view.service';

@Controller()
@Dependencies(ViewService)
export class ViewController {
  constructor(ViewService) {
    this.appService = ViewService;
  }
  @Get('/api')
  api() {
    return 'there should be api'
  }

  @Get('*')
  @Bind(Req(), Res())
  static(req, res) {
    const handle = this.appService.getNextServer().getRequestHandler();
    handle(req, res);
  }
}
