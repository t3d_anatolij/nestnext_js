import { Injectable } from '@nestjs/common';
import next from 'next';

@Injectable()
export class ViewService {

  async onModuleInit() {
    try {
      this.server = next({ dev: true, dir: './src/client' });
      await this.server.prepare();
    } catch (error) {
      console.log(error);
    }
  }

  getNextServer() {
    return this.server;
  }
}
