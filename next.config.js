var path = require('path');

module.exports = {
  async redirects() {
    return [
      {
        source: '/',
        destination: '/home',
        permanent: true,
      },
    ];
  },
  webpack: (config) => {
    config.resolve.alias.components = path.resolve(__dirname, 'src/client/components');

    return config;
  },
};